import { Coord } from "../interfaces/Coord";
import { Dimensions } from "../interfaces/Dimensions";

export const mapCoordToBoardIndex = (coord: Coord, dimensions: Dimensions): number => {
    const { x, y } = coord;
    const { width } = dimensions;

    return y * width + x;
};
