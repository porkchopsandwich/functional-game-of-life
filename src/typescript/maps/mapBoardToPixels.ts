import { Board } from "../interfaces/Board";
import { Config } from "../interfaces/Config";
import { Pixels } from "../interfaces/Pixels";
import { mapCoordToBoardIndex } from "./mapCoordToBoardIndex";

export const mapBoardToPixels = (config: Config, board: Board): Pixels => {
    const { scale } = config;
    const { width: boardWidth, height: boardHeight, values } = board;

    const pixelsWidth = boardWidth * scale;
    const pixelsHeight = boardHeight * scale;
    const pixels = new Uint8ClampedArray(pixelsWidth * pixelsHeight * 4);

    for (let boardY = 0; boardY < boardHeight; ++boardY) {
        for (let boardX = 0; boardX < boardWidth; ++boardX) {
            const boardIndex = mapCoordToBoardIndex({ x: boardX, y: boardY }, board);
            const value = values[boardIndex];
            const colour = value === 1 ? 0 : 255;

            const pixelsX = boardX * scale;
            const pixelsY = boardY * scale;
            for (let offsetY = 0; offsetY < scale; offsetY++) {
                for (let offsetX = 0; offsetX < scale; offsetX++) {
                    const pixelsIndex =
                        mapCoordToBoardIndex(
                            { x: pixelsX + offsetX, y: pixelsY + offsetY },
                            { width: pixelsWidth, height: pixelsHeight },
                        ) * 4;
                    pixels[pixelsIndex] = colour;
                    pixels[pixelsIndex + 1] = colour;
                    pixels[pixelsIndex + 2] = colour;
                    pixels[pixelsIndex + 3] = 255;
                }
            }
        }
    }

    return pixels;
};
