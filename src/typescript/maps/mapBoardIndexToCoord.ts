import { Board } from "../interfaces/Board";
import { Coord } from "../interfaces/Coord";

export const mapBoardIndexToCoord = (index: number, board: Board): Coord => {
    const { width } = board;

    const y = Math.floor(index / width);
    const x = index - y * width;

    return { x, y };
};
