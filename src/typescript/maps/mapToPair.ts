type PairMap<TInValue, TOutValue, TInKey extends string, TOutKey extends string> = Record<TInKey, TInValue> &
    Record<TOutKey, TOutValue>;

/**
 * Takes a pair of string keys, and a mapper, and returns a new mapper that invokes the original and returns a pair object containing both the input and output like:
 * {
 *     [inKey]: inValue,
 *     [outKey]: outValue
 * }
 *
 * @template {TInValue} the input value type.
 * @template {TOutValue} the output value type
 * @template {TInKey} describes the key to use in the pair for the input.
 * @template {TOutKey} describes the key to use in the pair for the output.
 *
 * @param {string} inKey
 * @param {string} outKey
 * @param {Function} mapper
 *
 * @returns {Function} The new mapper function which maps a {TInValue} to a {PairMap}.
 */
export const mapToPair = <TInValue, TOutValue, TInKey extends string, TOutKey extends string>(
    inKey: TInKey,
    outKey: TOutKey,
    mapper: (inValue: TInValue) => TOutValue,
) => {
    return (inValue: TInValue) => {
        return {
            [inKey]: inValue,
            [outKey]: mapper(inValue),
        } as PairMap<TInValue, TOutValue, TInKey, TOutKey>;
    };
};
