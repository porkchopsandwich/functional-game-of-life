import { range, map, Dictionary, has } from "ramda";
import { Board } from "../interfaces/Board";
import { Dimensions } from "../interfaces/Dimensions";
import { Seed } from "../interfaces/Seed";

type Input = Dimensions & {
    input: string;
};

const inputs: Dictionary<Input> = {
    block: {
        width: 4,
        height: 4,
        input: `0000
                0110
                0110
                0000`,
    },
    beehive: {
        width: 6,
        height: 5,
        input: `000000
                001100
                010010
                001100
                000000`,
    },

    rpentomino: {
        width: 50,
        height: 50,
        input: `00000
                00110
                01100
                00100
                00000`,
    },

    acorn: {
        width: 50,
        height: 50,
        input: `000000000
                001000000
                000010000
                011001110
                000000000`,
    },

    toad: {
        width: 6,
        height: 6,
        input: `000000
                000000
                001110
                011100
                000000
                000000`,
    },

    glider: {
        width: 10,
        height: 10,
        input: `0000000000
                0000000000
                0001000000
                0101000000
                0011000000
                0000000000
                0000000000
                0000000000
                0000000000
                0000000000`,
    },

    lwss: {
        width: 20,
        height: 7,
        input: `00000000000000000000
                00100100000000000000
                00000010000000000000
                00100010000000000000
                00011110000000000000
                00000000000000000000
                00000000000000000000`,
    }
};

const mapInputToBoard = (input: Input): Board => {
    const { width, height } = input;
    const preppedInput = input.input
        .replace(/[^\d\n]/gi, "")
        .split("\n")
        .map((line) => {
            const padLeft = Math.floor((width - line.length) / 2);
            const padRight = width - padLeft - line.length;
            return `${"0".repeat(padLeft)}${line}${"0".repeat(padRight)}`;
        })
        .reduce((acc, nextLine, index, original) => {
            const padTop = Math.floor((height - original.length) / 2);
            const padBottom = height - padTop - original.length;
            if (index === 0) {
                return [
                    ...range(0, padTop).map(() => {
                        return "0".repeat(width);
                    }),
                    ...acc,
                    nextLine,
                ];
            } else if (index === original.length - 1) {
                return [
                    ...acc,
                    nextLine,
                    ...range(0, padBottom).map(() => {
                        return "0".repeat(width);
                    }),
                ];
            } else {
                return [...acc, nextLine];
            }
        }, [] as string[])
        .join("");

    const values = Uint8Array.from(
        map((index) => {
            return parseInt(preppedInput[index], 10);
        }, range(0, width * height)),
    );

    return { width, height, values };
};

export const mapSeedToBoard = (seed: Seed = "block"): Board => {
    if (has(seed, inputs)) {
        return mapInputToBoard(inputs[seed]);
    }

    const width = 50;
    const height = 50;

    const values = Uint8Array.from(
        map(() => {
            return Math.round(Math.random());
        }, range(0, width * height)),
    );

    return { width, height, values };
};
