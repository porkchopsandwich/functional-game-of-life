import { Board } from "../interfaces/Board";
import { Config } from "../interfaces/Config";
import { mapBoardIndexToCoord } from "./mapBoardIndexToCoord";
import { mapCoordToBoardIndex } from "./mapCoordToBoardIndex";

const neighbouringOffsets = [
    [-1, -1],
    [0, -1],
    [1, -1],
    [-1, 0],
    [1, 0],
    [-1, 1],
    [0, 1],
    [1, 1],
];

export const mapBoardIndexToNeighbouringIndexes = (index: number, config: Config, board: Board): number[] => {
    const { x, y } = mapBoardIndexToCoord(index, board);
    const { width, height } = board;
    const { wrap } = config;

    return neighbouringOffsets.reduce((neighbours, [offsetX, offsetY]) => {
        let neighbourX = x + offsetX;
        let neighbourY = y + offsetY;

        if (wrap) {
            if (neighbourX < 0) {
                neighbourX += width;
            } else if (neighbourX >= width) {
                neighbourX -= width;
            }
            if (neighbourY < 0) {
                neighbourY += height;
            } else if (neighbourY >= height) {
                neighbourY -= height;
            }
        }

        if (neighbourX >= 0 && neighbourY >= 0 && neighbourX < width && neighbourY < height) {
            return [...neighbours, mapCoordToBoardIndex({ x: neighbourX, y: neighbourY }, board)];
        }

        return [...neighbours];
    }, [] as number[]);
};
