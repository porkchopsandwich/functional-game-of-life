import { Board } from "../interfaces/Board";
import { Config } from "../interfaces/Config";
import { mapBoardIndexToNeighbouringIndexes } from "./mapBoardIndexToNeighbouringIndexes";

export const mapBoardToNextBoard = (config: Config, board: Readonly<Board>): Board => {
    const { width, height, values } = board;

    // Any live cell with two or three neighbors survives.
    // Any dead cell with three live neighbors becomes a live cell.
    // All other live cells die in the next generation. Similarly, all other dead cells stay dead.
    const nextValues = values.map((value, index) => {
        const livingNeighbours = mapBoardIndexToNeighbouringIndexes(index, config, board).reduce(
            (living, neighbourIndex) => {
                return living + (values[neighbourIndex] > 0 ? 1 : 0);
            },
            0,
        );

        if ((livingNeighbours === 2 || livingNeighbours === 3) && value === 1) {
            return 1;
        } else if (livingNeighbours === 3 && value === 0) {
            return 1;
        }

        return 0;
    });

    return { width, height, values: nextValues };
};
