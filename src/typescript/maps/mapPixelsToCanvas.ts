import { Board } from "../interfaces/Board";
import { Config } from "../interfaces/Config";
import { Pixels } from "../interfaces/Pixels";

export const mapPixelsToCanvas = (config: Config, board: Board, pixels: Pixels) => {
    const { width, height } = board;
    const { scale, canvas, context } = config;

    canvas.width = width * scale;
    canvas.height = height * scale;

    context.fillStyle = "#000";
    context.fillRect(0, 0, canvas.width, canvas.height);

    const imageData = context.getImageData(0, 0, canvas.width, canvas.height);
    imageData.data.set(pixels);
    context.putImageData(imageData, 0, 0);
    return canvas;
};
