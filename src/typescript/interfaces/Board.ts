import { Dimensions } from "./Dimensions";

export type Board = Dimensions &
    Readonly<{
        values: Uint8Array;
    }>;
