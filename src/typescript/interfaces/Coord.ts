export type Coord = Readonly<{
    x: number;
    y: number;
}>;
