export type Config = Readonly<{
    scale: number;
    wrap: boolean;
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    updateInterval: number;
}>;
