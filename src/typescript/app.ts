import { always, last, partial, partialRight, split, unary } from "ramda";
import { BehaviorSubject, combineLatest, interval, merge, of } from "rxjs";
import { debounceTime, distinctUntilChanged, map, pluck, scan, switchMap } from "rxjs/operators";
import { Config } from "./interfaces/Config";
import { mapBoardToNextBoard } from "./maps/mapBoardToNextBoard";
import { mapBoardToPixels } from "./maps/mapBoardToPixels";
import { mapPixelsToCanvas } from "./maps/mapPixelsToCanvas";
import { mapSeedToBoard } from "./maps/mapSeedToBoard";
import { mapToPair } from "./maps/mapToPair";
import { domIsReady } from "./utils/domIsReady";
import { fromEventOnce } from "./utils/fromEventOnce";

(async () => {
    // ----------------------------------------------------
    // Observables from DOM elements
    // ----------------------------------------------------

    // Note that we conceptualise these as a stream of discrete Events.
    // The actual value that is derived from these Events is a value over time, in the form of a BehaviorSubject, below

    // Seed from the text box
    const seedInputKeyup$ = fromEventOnce(document, "#seed", "keyup").pipe(
        debounceTime(100), // Fire once per 100MS
        pluck<Event, HTMLInputElement>("target"), // Pluck the target from the event, cast to HTML Input
        pluck("value"), // Pluck the value from the target
        distinctUntilChanged(), // Ignore non character inputs
    );

    // Seed when clicked from predefined options
    const seedClick$ = fromEventOnce(document, "#seeds a", "click").pipe(
        pluck<Event, HTMLAnchorElement>("target"), // Pluck the target, cast as an Anchor
        pluck("href"), // Pluck the href from the anchor
        map(split("/")), // Split by slash
        map<string[], string>(last), // Select the last item in the split list
    );

    // Scale from the input
    const scaleInputChange$ = fromEventOnce(document, "#scale", "change").pipe(
        pluck<Event, HTMLInputElement>("target"), // Pluck the target as an Input
        pluck("value"), // Pluck the value
        distinctUntilChanged(), // Ignore invalid inputs
        map(unary(partialRight(parseInt, [10]))), // Parse to int (curry the radix, and force function to by unary)
    );

    // Pause/play state from the button
    const isPlayingClick$ = fromEventOnce(document, "#playPause", "click").pipe(
        map(always(true)),
        scan((acc, value) => {
            return !acc;
        }, true),
        distinctUntilChanged(),
    );

    // Update interval from the input
    const updateIntervalChange$ = fromEventOnce(document, "#updateInterval", "change").pipe(
        pluck<Event, HTMLInputElement>("target"),
        pluck("value"),
        distinctUntilChanged(),
        map(unary(partialRight(parseInt, [10]))),
    );

    // Wrap toggle from the input
    const wrapToggleClick$ = fromEventOnce(document, "#wrap", "click").pipe(
        pluck<Event, HTMLInputElement>("target"),
        pluck("checked"),
        distinctUntilChanged(),
    );

    // ----------------------------------------------------
    // Create subjects to multicast the events
    // ----------------------------------------------------

    // Each subscription to an Observable triggers the evaluation of the observable's 'stack'.
    // For values over time, we can use a BehaviorSubject to multicast to many subscribers
    // As this Subject is the only subscriber to the root Observable, its stack will be evaluated once.

    const scale$ = new BehaviorSubject(10);
    scaleInputChange$.subscribe(scale$);

    const isPlaying$ = new BehaviorSubject(true);
    isPlayingClick$.subscribe(isPlaying$);

    const updateInterval$ = new BehaviorSubject(50);
    updateIntervalChange$.subscribe(updateInterval$);

    const wrap$ = new BehaviorSubject(true);
    wrapToggleClick$.subscribe(wrap$);

    // ----------------------------------------------------
    // Canvas and configuration for the board
    // ----------------------------------------------------

    // Canvas and context
    const canvasAndContext$ = of(document.createElement("canvas")).pipe(
        map(
            // Link the canvas and its context in a tuple
            mapToPair("canvas", "context", (canvas) => {
                return canvas.getContext("2d") as CanvasRenderingContext2D;
            }),
        ),
    );

    // Build config by combining observables
    const config$ = combineLatest([scale$, wrap$, canvasAndContext$, updateInterval$]).pipe(
        map(
            ([scale, wrap, { canvas, context }, updateInterval]): Config => {
                return { scale, wrap, canvas, context, updateInterval };
            },
        ),
    );

    // ----------------------------------------------------
    // Derive the seed, initial board state, and current build state
    // ----------------------------------------------------

    // Take the latest seed from either source (clicked links or text input)
    const seed$ = merge(seedInputKeyup$, seedClick$);

    // A stream of boards from the seeds
    const initialBoard$ = seed$.pipe(map(unary(mapSeedToBoard)));

    // An interval, based on the update interval value
    const interval$ = combineLatest([config$, isPlaying$]).pipe(
        switchMap(([config, isPlaying]) => {
            if (isPlaying) {
                return interval(config.updateInterval);
            } else {
                return of(0);
            }
        }),
    );

    // A stream of the board/config being updated on an interval
    const board$ = combineLatest([initialBoard$, config$]).pipe(
        switchMap(([initialBoard, config]) => {
            return interval$.pipe(scan(unary(partial(mapBoardToNextBoard, [config])), initialBoard));
        }),
    );

    // ----------------------------------------------------
    // Rendered board
    // ----------------------------------------------------

    // Combine boards and configs into a 3-tuple with the resulting pixels
    const pixelsConfigAndBoard$ = combineLatest([board$, config$]).pipe(
        map(([board, config]) => {
            return {
                board,
                config,
                pixels: mapBoardToPixels(config, board),
            };
        }),
    );

    // A stream of canvases representing the rendered board
    const renderedCanvas$ = pixelsConfigAndBoard$.pipe(
        map(({ board, pixels, config }) => {
            return mapPixelsToCanvas(config, board, pixels);
        }),
    );

    // ----------------------------------------------------
    // Reflect the observables back into the DOM
    // ----------------------------------------------------

    // Wait for the DOM
    await domIsReady;

    const target = document.querySelector("#canvas") as HTMLDivElement;

    // Reflect the seed stream back onto the DOM
    seed$.subscribe((seed) => {
        (document.querySelector("#seed") as HTMLInputElement).value = seed;
    });

    // Reflect the scale stream back onto the DOM
    scale$.subscribe((scale) => {
        (document.querySelector("#scale") as HTMLInputElement).value = `${scale}`;
    });

    // Reflect the scale stream back onto the DOM
    updateInterval$.subscribe((scale) => {
        (document.querySelector("#updateInterval") as HTMLInputElement).value = `${scale}`;
    });

    // Reflect the wrap stream back onto the DOM
    wrapToggleClick$.subscribe((wrap) => {
        setTimeout(() => {
            (document.querySelector("#wrap") as HTMLInputElement).checked = wrap;
        }, 0);
    });

    // Reflect is playing status back onto the DOM
    isPlaying$.subscribe((isPlaying) => {
        (document.querySelector("#playPause") as HTMLButtonElement).textContent = isPlaying ? "Pause" : "Play";
    });

    // Render the canvas
    canvasAndContext$.subscribe(({ canvas }) => {
        target.innerHTML = "";
        target.append(canvas);
    });

    // Subscribe to the rendered canvas to kick things off
    // Note that this is something of an anti-pattern, necessary because mapPixelsToCanvas is impure (it updates the canvas content directly)
    renderedCanvas$.subscribe();
})();
