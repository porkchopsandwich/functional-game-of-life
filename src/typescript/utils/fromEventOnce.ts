import { fromEvent, Observable } from "rxjs";
import { filter, map, share } from "rxjs/operators";

export const fromEventOnce = <T extends Event>(
    element: Document | Element,
    selector: string,
    eventName: string,
): Observable<T> => {
    return fromEvent(element, eventName).pipe(
        share(),
        map((event: Event) => {
            event.preventDefault();
            return { event, delegate: (event.target as Element).closest(selector) };
        }),
        filter((x) => {
            return x.delegate !== null;
        }),
        map((x) => {
            return x.event as T;
        }),
    );
};
