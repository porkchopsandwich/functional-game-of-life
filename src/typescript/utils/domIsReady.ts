import { domReady } from "./domReady";

export const domIsReady = new Promise((resolve) => {
    domReady(resolve);
});
