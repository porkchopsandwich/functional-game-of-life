module.exports = {
    // Use the Typescript parser
    parser: "@typescript-eslint/parser",

    // Start from the recommended rules from the @typescript-eslint/eslint-plugin
    extends: ["plugin:unicorn/recommended", "plugin:@typescript-eslint/recommended", "prettier/@typescript-eslint", "plugin:prettier/recommended"],

    parserOptions: {
        // Allows for the parsing of modern ECMAScript features
        ecmaVersion: 2018,
        // Allows for the use of imports
        sourceType: "module",
        ecmaFeatures: {
            jsx: true,
        },
    },

    // Customize the rules
    rules: {
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/interface-name-prefix": ["error", { prefixWithI: "always" }],
        "@typescript-eslint/no-empty-interface": "off",
        "no-console": "error",
        "unicorn/filename-case": "off",
        "unicorn/prevent-abbreviations": "off",
        "unicorn/consistent-function-scoping": "off",
        "unicorn/number-literal-case": "off",
    },

    settings: {
    },
};
